package com.vendingmachine.report;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Queue;

import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import com.vendingmachine.machine.product.Product;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;


public class PDFGenerator {

    private static final String FILE_PATH = "src/main/resources/weeklyReport.pdf";

    public void generatePDF(Queue<Product> dispensedProductsQueue) throws FileNotFoundException {
        DateTimeFormatter startFormatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm:ss");


            //PdfWriter.getInstance(document, new FileOutputStream(new File(FILE_PATH + "/test.pdf")));
            PdfWriter writer = new PdfWriter(FILE_PATH);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);

            Paragraph p = new Paragraph();
            p.add("VENDING MACHINE REPORT");
            document.add(p);
            Paragraph p2 = new Paragraph();
            p2.add("Below, the report for last week dispensed products");
            document.add(p2);
            float[] pointColumnWidths = {150F, 150F, 150F};
            Table table = new Table(pointColumnWidths);

            // Adding cells to the table
            double totalPrice = 0;
            table.addCell(new Cell().add("Product ID"));
            table.addCell(new Cell().add("Pret"));
            table.addCell(new Cell().add("Data"));
            for (Product product : dispensedProductsQueue) {
                totalPrice += product.getPrice();
                table.addCell(new Cell().add(product.getId() + ""));
                table.addCell(new Cell().add(product.getPrice() + ""));
                table.addCell(new Cell().add(product.getDispensalDate().format(startFormatter)));
            }
            table.addCell("TOTAL");
            table.addCell(totalPrice+"");
            table.addCell("");
            // Adding Table to document
            document.add(table);

            // close
            document.close();
            System.out.println("Done");

    }
}