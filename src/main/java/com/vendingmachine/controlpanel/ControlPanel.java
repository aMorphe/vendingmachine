package com.vendingmachine.controlpanel;

import com.vendingmachine.machine.IVendingMachine;
import com.vendingmachine.machine.product.Product;

public class ControlPanel {
    private IVendingMachine iVendingMachine;

    public Product dispenseProduct(int shelfCode) {
        return iVendingMachine.dispenseProduct(shelfCode);
    }

    public IVendingMachine getiVendingMachine() {
        return iVendingMachine;
    }

    public void setiVendingMachine(IVendingMachine iVendingMachine) {
        this.iVendingMachine = iVendingMachine;
    }
}
