package com.vendingmachine.machine.storage;

import com.vendingmachine.machine.product.Product;

import java.util.Map;
import java.util.Queue;

public class Storage {
    private  Map<Integer, Queue<Product>> storageMap;

    public  Map<Integer, Queue<Product>> getStorageMap() {
        return storageMap;
    }

    public  void setStorageMap(Map<Integer, Queue<Product>> storageMap) {
        this.storageMap = storageMap;
    }


}
