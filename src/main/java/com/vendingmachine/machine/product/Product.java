package com.vendingmachine.machine.product;

import java.time.LocalDateTime;

public abstract class Product {
    private int id;
    private double price;
    private LocalDateTime dispensalDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public LocalDateTime getDispensalDate() {
        return dispensalDate;
    }

    public void setDispensalDate(LocalDateTime dispensalDate) {
        this.dispensalDate = dispensalDate;
    }
}
