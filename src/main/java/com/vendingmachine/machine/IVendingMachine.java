package com.vendingmachine.machine;

import com.vendingmachine.bank.Money;
import com.vendingmachine.machine.product.Product;

import java.util.Queue;

public interface IVendingMachine {
    public Product dispenseProduct(int shelfCode);
    public boolean payProductPrice(int shelfCode, double amountPaid);
    public Queue<Money> returnChange(double amountReceived, double amountExpected);
    public boolean validateAmountReceived (Queue<Money> receivedAmount);
}
