package com.vendingmachine.machine;

import com.vendingmachine.bank.Money;
import com.vendingmachine.exception.NotFullPaidException;
import com.vendingmachine.exception.SoldOutException;
import com.vendingmachine.machine.product.Product;
import com.vendingmachine.machine.storage.Storage;

import java.util.ArrayDeque;
import java.util.Map;
import java.util.Queue;

public class VendingMachineService implements IVendingMachine {
    private Storage storage;

    public Product dispenseProduct(int shelfCode) {
        Map<Integer, Queue<Product>> storageMap = storage.getStorageMap();
        Queue<Product> productQueue = storageMap.get(shelfCode);
        if(productQueue.isEmpty()){
            throw new SoldOutException("The product is not available.");
        }
        return storageMap.get(shelfCode).poll();
    }

    @Override
    public boolean payProductPrice(int shelfCode, double amountPaid) {
        Map<Integer, Queue<Product>> storageMap = storage.getStorageMap();
        Queue<Product> productQueue = storageMap.get(shelfCode);
        double productPrice = productQueue.peek().getPrice();
        if (amountPaid >= productPrice) {
            //todo return restul de bani
            dispenseProduct(shelfCode);
            return true;
        } else {
            throw new NotFullPaidException("The amount of money introduced is insufficent!");
        }
    }

    @Override
    public Queue<Money> returnChange(double amountReceived, double amountExpected) {
        //amount received = 11
        //amount expected = 6
        //amount change = 5
        double totalChange = amountReceived - amountExpected;
        double change = totalChange % 5;
        double noOfCoins = totalChange / 5;
        Queue<Money> coins = new ArrayDeque<>();
        if (noOfCoins != 0) {
            for (int i = 0; i <= noOfCoins; i++) {
                coins.add(Money.FIVE_DOLLAR);
            }
        }
        if (change == 0) {
            return coins;
        } else {
            change = change % 1;
            noOfCoins = change / 1;
            if (noOfCoins != 0) {
                for (int i = 0; i <= noOfCoins; i++) {
                    coins.add(Money.ONE_DOLLAR);
                }
            }
            if (change == 0) {
                return coins;
            } else {
                change = change % 0.5;
                noOfCoins = change / 0.5;
                if (noOfCoins != 0) {
                    for (int i = 0; i <= noOfCoins; i++) {
                        coins.add(Money.FIFTY_CENT);
                    }
                }
                if (change == 0) {
                    return coins;
                } else {
                    change = change % 0.1;
                    noOfCoins = change / 0.1;
                    if (noOfCoins != 0) {
                        for (int i = 0; i <= noOfCoins; i++) {
                            coins.add(Money.TEN_CENT);
                        }
                    }
                    if (change == 0) {
                        return coins;
                    }
                }
            }
        }
        return coins;
    }

    // todo metoda care verifica in  storage daca produsul e disponibil
    // todo metoda care verifica in Bank daca avem rest disponibil
    // todo metoda care valideaza banii introdusi de user

    @Override
    public boolean validateAmountReceived(Queue<Money> receivedAmount) {

        return false;
    }

    public Storage getStorage() {
        return storage;
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }
}
