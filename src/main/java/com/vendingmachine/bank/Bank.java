package com.vendingmachine.bank;

import java.util.Queue;

public class Bank {
    private int id;
    private Queue<Money> tenCentStack;
    private Queue<Money> fiftyCentStack;
    private Queue<Money> oneDollarStack;
    private Queue<Money> fiveDollarStack;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Queue<Money> getTenCentStack() {
        return tenCentStack;
    }

    public void setTenCentStack(Queue<Money> tenCentStack) {
        this.tenCentStack = tenCentStack;
    }

    public Queue<Money> getFiftyCentStack() {
        return fiftyCentStack;
    }

    public void setFiftyCentStack(Queue<Money> fiftyCentStack) {
        this.fiftyCentStack = fiftyCentStack;
    }

    public Queue<Money> getOneDollarStack() {
        return oneDollarStack;
    }

    public void setOneDollarStack(Queue<Money> oneDollarStack) {
        this.oneDollarStack = oneDollarStack;
    }

    public Queue<Money> getFiveDollarStack() {
        return fiveDollarStack;
    }

    public void setFiveDollarStack(Queue<Money> fiveDollarStack) {
        this.fiveDollarStack = fiveDollarStack;
    }
}
