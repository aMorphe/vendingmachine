package vendingmachine;

import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ExecutorTest {
    @Test
    public void ExecutorTest(){
        ScheduledExecutorService ses= Executors.newScheduledThreadPool(1);
        Runnable task2 =()-> System.out.println("Running the second task");
        task1();
        ses.schedule(task2,5, TimeUnit.SECONDS);

    }
    public void task1(){
        System.out.println("Running task1...");
    }
    public void task2(){
        System.out.println("Running task2...");
    }
}
