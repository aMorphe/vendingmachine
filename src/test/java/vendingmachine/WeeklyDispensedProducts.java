package vendingmachine;

import com.vendingmachine.controlpanel.ControlPanel;
import com.vendingmachine.machine.VendingMachineService;
import com.vendingmachine.machine.product.CocaCola;
import com.vendingmachine.machine.product.Product;
import com.vendingmachine.machine.storage.Storage;
import com.vendingmachine.report.PDFGenerator;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

public class WeeklyDispensedProducts {
    @Test
    public void sendWeeklyReport() throws FileNotFoundException {
        DateTimeFormatter startFormatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");

        VendingMachineService iVendingMachine = new VendingMachineService();
        Storage storage = new Storage();
        Map<Integer, Queue<Product>> storageMap = new HashMap<Integer, Queue<Product>>();
        Product p1 = new CocaCola();
        p1.setId(1);
        p1.setPrice(32);
        Product p2 = new CocaCola();
        p2.setId(2);
        p2.setPrice(21);
        Product p3 = new CocaCola();
        p3.setId(3);
        p3.setPrice(6);
        Product p4 = new CocaCola();
        p4.setId(4);
        p4.setPrice(89);
        Product p5 = new CocaCola();
        p5.setId(5);
        p5.setPrice(89);
        Queue<Product> productQueue = new ArrayDeque<Product>();
        productQueue.add(p1);
        productQueue.add(p2);
        productQueue.add(p3);
        productQueue.add(p4);
        productQueue.add(p5);

        storageMap.put(3,productQueue);

        ControlPanel controlPanel = new ControlPanel();
        controlPanel.setiVendingMachine(iVendingMachine);
        storage.setStorageMap(storageMap);
        iVendingMachine.setStorage(storage);

        Queue<Product> dispensedProductsQueue = new ArrayDeque<Product>();
        for(int i =0; i<5; i++) {
            Product dispensedProduct = controlPanel.dispenseProduct(3);
            dispensedProduct.setDispensalDate(LocalDateTime.now());
            dispensedProductsQueue.add(dispensedProduct);
        }
        PDFGenerator generator = new PDFGenerator();
        generator.generatePDF(dispensedProductsQueue);
    }
}
