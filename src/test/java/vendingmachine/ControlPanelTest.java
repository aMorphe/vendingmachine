package vendingmachine;

import com.vendingmachine.controlpanel.ControlPanel;
import com.vendingmachine.email.EmailSender;
import com.vendingmachine.machine.IVendingMachine;
import com.vendingmachine.machine.VendingMachineService;
import com.vendingmachine.machine.product.CocaCola;
import com.vendingmachine.machine.product.Product;
import com.vendingmachine.machine.storage.Storage;
import com.vendingmachine.report.PDFGenerator;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class ControlPanelTest {
    @Test
    public void runInALoop(){

    }
    @Test
    public void dispenseProductTest() {
        VendingMachineService iVendingMachine = new VendingMachineService();
        Storage storage = new Storage();
        Map<Integer, Queue<Product>> storageMap = new HashMap<Integer, Queue<Product>>();
        Product p1 = new CocaCola();
        p1.setId(1);
        Product p2 = new CocaCola();
        p2.setId(2);
        Queue<Product> productQueue = new ArrayDeque<Product>();
        productQueue.add(p1);
        productQueue.add(p2);
        storageMap.put(3,productQueue);
        ControlPanel controlPanel = new ControlPanel();
        controlPanel.setiVendingMachine(iVendingMachine);
        storage.setStorageMap(storageMap);
        iVendingMachine.setStorage(storage);

        Product dispensedProduct = controlPanel.dispenseProduct(3);

        DateTimeFormatter startFormatter = DateTimeFormatter.ofPattern("dd-MM-yy HH:mm");
        String time = LocalDateTime.now().format(startFormatter);
        StringBuilder output = new StringBuilder();
        dispensedProduct.setPrice(5.00);
        output.append("Produsul cu id-ul ").append(dispensedProduct.getId()).append(" si pretul ").append(dispensedProduct.getPrice()).append(" a fost cuparat la ").append(time);
        PDFGenerator generator = new PDFGenerator();
        EmailSender testSender = new EmailSender();
        testSender.sendEmail("alan.morphe@yandex.com");




        assertNotNull(dispensedProduct);
        assertEquals(1,dispensedProduct.getId());
    }
}



