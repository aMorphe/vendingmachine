package vendingmachine;

import com.vendingmachine.email.EmailSender;
import org.junit.Test;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class EmailSenderTest {
    @Test
    public void loopSending(){
        ScheduledExecutorService ses = Executors.newScheduledThreadPool(1);
        Runnable loopTask = () -> testTheMail();
        ScheduledFuture<?> scheduledFuture= ses.scheduleAtFixedRate(loopTask,0,5, TimeUnit.DAYS);
    }

    @Test
    public void testTheMail(){
        EmailSender testSender = new EmailSender();
        testSender.sendEmail("zanc.razvan@gmail.com");
    }
}
